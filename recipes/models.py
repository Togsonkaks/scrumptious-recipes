from django.db import models


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now=False)
    updated = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.name + " by " + self.author


# class Recipe2(models.Model):
#     name = models.CharField(max_length=125)
#     author = models.CharField(max_length=100)
#     description = models.TextField()
#     image = models.URLField(null=True, blank=True)
#     created = models.DateTimeField(auto_now=True)
#     updated = models.DateTimeField(auto_now=True)

# class Comment(models.Model):
#     content = models.TextFielD()
#     author = models.CharField(max_length=50)
#     post = models.ForeignKey("Recipe", on_delete=models.CASCADE)

#     def __str__(self):
#         return f"Comment by {self.author}"